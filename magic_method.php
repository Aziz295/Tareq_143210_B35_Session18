<?php


class ImAClass{



    public static $mymsg;
    public static function massage(){
        echo self::$mymsg."<br>";
    }

    public function __construct($value){
        echo $value."<br>";
    }
    public function __destruct()
    {
        echo "Good Bye<br>";
    }
    public function __call($name, $arguments)
    {
        echo $name."<br>";
        print_r($arguments);
    }
    public static function __callStatic($name, $arguments)
    {
        echo $name."<br>";
        print_r($arguments);
    }
    public function __get($name)
    {
        echo $name."<br>";
    }
    public function __set($name, $value)
    {
        echo $name."<br>";
        echo $value."<br>";
    }
    public function __isset($name)
    {
        echo $name."<br>";
    }
    public function __unset($name)
    {
        echo $name."<br>";
    }
   /* public function __sleep()
    {
        echo "I'm sleeping";
    }*/
}
ImAClass::$mymsg="Data has been deleted"; //static massage.
ImAClass::massage(); //static massage
$obj = new ImAClass("Hello World!"); //construct
$obj->show(22,56,"Hello"); //call
$obj = new ImAClass("Hello! Class Initialed");//get
echo $obj->window;//get
$obj->door = "Eta 1ta dorja";//set
isset($obj->chair);//isset
unset($obj->table);//unset

?>
